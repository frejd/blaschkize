/*
/*
 * Copyright (c) 2021 Frej Dahlin <frej.weistrom_dahlin@math.lu.se>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <arpa/inet.h>

#include <complex.h>
#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

#include "../complot/complot.h"

#define ITOC(i, j) (-1 +     (double)(j) * 2 / (DIM - 1) + \
                     I - I * (double)(i) * 2 / (DIM - 1))
#define LEN(x) (sizeof (x) / sizeof *(x))
#define RANDF() (2 * (double)rand() / (double)RAND_MAX - 1.0)
#define SQUARE(x) ((x) * (x))

#define D 16
#define DIM 64
#define NZEROS 256
#define NSTOCH 4

void integratedensity(int, int);	
double sigmoidprime(double);

complex double gradient[NZEROS];
complex double zeros[NZEROS];
double learnrate = 1;
int mask[DIM][DIM] = {0}, stochmask[NZEROS] = {0};

complex double
blaschkeprod(complex double z)
{
	complex double a = 1, b = 1;
	int i;

	for (i = 0; i < NZEROS; i++) {
		a *= z - zeros[i];
		b *= 1 - conj(zeros[i]) * z;
	}
	return a / b; /* it is faster to compute a and b separately */
}

void
computegradient()
{
	int i, j;

	memset(gradient, 0, sizeof(gradient));
	for (i = 0; i < DIM; i++) {
		for (j = 0; j < DIM; j++) {
			integratedensity(i, j);
		}
	}
}

double
error()
{
	int b, e = 0, i, j;

	for (i = 0; i < DIM; i++)
		for (j = 0; j < DIM; j++) {
			b = cimag(blaschkeprod(ITOC(i, j))) > 0 ? 0 : 1;
			e += b == mask[i][j] ? 0 : 1;
		}
	return (double)e / SQUARE(DIM);
}

void
gradientdescent()
{
	double len = 0;
	int k;

	for (k = 0; k < NZEROS; k++) {
		if (!stochmask[k])
			continue;
		len += SQUARE(cabs(gradient[k]));
	}
	/*
	 * For some reason the convergence is quicker if one does not take the
	 * square root of the len.
	 */
	/*len = sqrt(len) + 0.0001;*/
	for (k = 0; k < NZEROS; k++)
		zeros[k] -= learnrate * gradient[k] / len;
}

void
integratedensity(int i, int j)
{
	complex double b, bk, bbark, z;
	int k;

	z = ITOC(i, j);
	b = blaschkeprod(z);
	for (k = 0; k < NZEROS; k++) {
		if (!stochmask[k])
			continue;
		bk = z * b / (1 - conj(zeros[k]) * z);
		bbark = conj(b / (z - zeros[k]));
		gradient[k] += (mask[i][j] ? 1 : -1) *
		    -I * (bk + bbark) * sigmoidprime(D * cimag(b));
	}
}


void
randomizezeros()
{
	int i;

	for (i = 0; i < NZEROS; i++)
		zeros[i] = RANDF() + I * RANDF();
}

int
readmask(const char *filename)
{
	FILE *img;
	uint32_t hdr[4], width, height, i, j;
	uint16_t rgba[4];

	if (!(img = fopen(filename, "r"))) {
		fprintf(stderr, "gdod: fopen: %s\n", strerror(errno));
		return 1;
	}
	/* read header */
	if (fread(hdr, sizeof(*hdr), LEN(hdr), img) != LEN(hdr))
		goto readerr;
	if (memcmp("farbfeld", hdr, sizeof("farbfeld") - 1)) {
		fprintf(stderr, "graddesc: invalid magic value\n");
		return 1;
	}
	width = ntohl(hdr[2]);
	height = ntohl(hdr[3]);
	
	for (i = 0; i < height; i++) {
		for (j = 0; j < width; j++) {
			if (fread(rgba, sizeof(*rgba), LEN(rgba),
			    img) != LEN(rgba))
				goto readerr;
			if (rgba[0] || rgba[1] || rgba[2])
				mask[i][j] = 1;
			else
				mask[i][j] = 0;
		}
	}

	/* clean up */
	if (fclose(img)) {
		fprintf(stderr, "graddesc: fclose: %s\n", strerror(errno));
		return 1;
	}
	
	return 0;
readerr:
	fprintf(stderr, "graddesc: fread: Unexpected EOF\n");
	return 1;
}

int
readzeros()
{
	FILE *file;
	
	if (!(file = fopen("zeros", "r"))) {
		fprintf(stderr, "gdod: fopen: %s\n", strerror(errno));
		return 1;
	}
	if (fread(zeros, sizeof(*zeros), LEN(zeros), file) != LEN(zeros)) {
		fprintf(stderr, "gdod: fread: Unexpected EOF\n"); 
		return 1;
	}
	if (fclose(file)) {
		fprintf(stderr, "gdod: fclose: %s\n", strerror(errno));
		return 1;
	}
	return 0;
}

double
sigmoidprime(double x)
{
	if (isinf(pow(M_E, -x)))
		return 0;
	return pow(M_E, -x) / SQUARE(1 + pow(M_E, -x));
}

int
writezeros()
{
	FILE *file;

	if (!(file = fopen("zeros", "w"))) {
		fprintf(stderr, "gdod: fopen: %s\n", strerror(errno));
		return 1;
	}
	if(fwrite(zeros, sizeof(*zeros), LEN(zeros), file) != LEN(zeros)) {
		fprintf(stderr, "graddesc: fwrite: %s\n", strerror(errno));
		return 1;
	}
	if (fclose(file)) {
		fprintf(stderr, "graddesc: fclose: %s\n", strerror(errno));
		return 1;
	}
	return 0;
}

void
computestochmask()
{
	int i, j;
	
	memset(stochmask, 0, sizeof(stochmask));
	for (i = 0; i < NSTOCH; i++) {
		j = rand() % NZEROS;
		if (stochmask[j])
			i--;
		else
			stochmask[j] = 1;
	}
}

int
main(int argc, char *argv[])
{
	FILE *img;
	double e = 1, mine = 1, targe = 0;
	int i, j, k;

	if (readmask(argv[1]))
		return 1;
	srand(time(0));
	randomizezeros();	
	
	for (i = 0; e > targe; i++) {
		computestochmask();
		computegradient();
		learnrate = NSTOCH * e;
		gradientdescent();
		e = error();
		if (e < mine) {
			if (!(img = fopen("blaschkize.ff", "w")))
				err(1, "fopen");
			mine= e;
			complot_unisq(blaschkeprod, DIM, BINARY, img);
			writezeros();
			usleep(1000 * 50);
		}
		printf("err: %d: %f: minerr: %f lr: %f\n",
		    i, e, mine, learnrate);
	}
}
